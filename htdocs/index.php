<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>A03_VIGIL</title>
<!--<style type="text/css">
	body{
		background: #000000;
		color:#FF0004;
		font-family: papyrus, fantasy;
		max-width: 900;
		max-height: none;
	}
	
	h1{
		text-align: center;
	}
	
	.selection3Selector{
		color:#4B3BFF;
		text-align: right;
		border: thick;
		border-color: white;
	}
	
	.fontStyle1{
		font-family: Cambria, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", "serif";
	}
	
	.fontStyle2{
		font-family: Gotham, "Helvetica Neue", Helvetica, Arial, "sans-serif";
	}
	
	.fontStyle3{
		font-family:"Gill Sans", "Gill Sans MT", "Myriad Pro", "DejaVu Sans Condensed", Helvetica, Arial, "sans-serif"
	}
	
	#menu{
		background-color: #dddddd;
		width: 120px;
		display: block;
		text-decoration:overline;
	}
	
	
</style>-->
<link href="A03_Vigil_CSS_Sheet.css" rel="stylesheet" type="text/css">
</head>

<body>
	<h1 id="header">Assigment 3-Vigil, Dmitri</h1>
	
	<ul id="menu">
		<li><a href="#s1">Selection 1</a></li>
		<li><a href="#s2">Selection 2</a></li>
		<li><a href="#s3">Selection 3</a></li>
		<li><a href="#s4">Selection 4</a></li>
		<li><a href="#s5">Selection 5</a></li>
		<li><a href="#s6">Selection 6</a></li>
	</ul>
	
	<ul id="s1">
		<li>Milk</li>
		<li>Eggs</li>
		<li>Bread</li>
		<li>Cheese</li>
		<li>Deli meat</li>
	</ul>
	<p><a href="#header">Back to top</a></p>

	<ul id="s2">Ohio<ul>
		<li>Columbus</li>
		<li>Cleveland</li>
		<li>Cincinnati</li>
		<li>Toledo</li>
	  </ul></ul>
	  <ul>Michigan<ul>
		<li>Detroit</li>
		<li>Flint</li>
		<li>Ann Harbor</li>
		<li>East Lansing</li>
	  </ul></ul>
	  <ul>Colorado<ul>
		<li>Denver</li>
		<li>Colorado Spring</li>
		<li>Fort Collins</li>
		<li>Border</li>
	  </ul></ul>
	  <ul>New York<ul>
			<li>New York</li>
		<li>Buffalo</li>
		<li>Albany</li>
		<li>Utica</li>
	  </ul></ul>
	<p><a href="#header">Back to top</a></p>

		
	<dl class="selection3Selector" id="s3"><dt>The Dark Knight</dt>
	  <dd>After a year as Batman Bruce must now take on his biggest foe in the Joker</dd>
	  <dt>Monty Python and the Holy Grail</dt>
	  <dd>King Author and his knights look for the Holy Grail in Monty Python fastion</dd>
	  <dt>Blade Runner: 2049</dt>
	  <dd>In 2049 a blade runner discovers that he might be the son of Dicker from the first movie</dd>
	  <dt>The Lion King</dt>
	  <dd>It Hamlet but with lions</dd>
	  <dt>Jurricuss Park</dt>
	  <dd>Dinosuras have been recreated in a lab, but was it the right think to do?</dd>
	</dl>
		<p><a href="#header">Back to top</a></p>

	
<table width="200" border="1" id="s4">
  <tbody>
	  <tr><th colspan="5">Dog Breed</th></tr>
    <tr>
      <th scope="col">&nbsp;</th>
      <th scope="col">Retriver</th>
      <th scope="col">Bulldog</th>
      <th scope="col">German Shorthair</th>
      <th scope="col">Boxer</th>
    </tr>
    <tr>
      <td><p>&nbsp;</p>
      <p><strong>Average Height</strong></p></td>
      <td>1km</td>
      <td>5km</td>
      <td>3km</td>
      <td>4km</td>
    </tr>
    <tr>
      <td><strong>Average Weight</strong></td>
      <td>400kg</td>
      <td>800kg</td>
      <td>900kg</td>
      <td>420kg</td>
    </tr>
    <tr>
      <td><strong>Dog Greed</strong></td>
      <td>This</td>
      <td>Is</td>
      <td>Very</td>
      <td>Silly</td>
    </tr>
  </tbody>
</table>
	<p><a href="#header">Back to top</a></p>

	
<table width="200" border="1" id="s5">
  <tbody>
    <tr>
      <th scope="col">&nbsp;</th>
      <th scope="col">Ohio</th>
      <th scope="col">Michigan</th>
      <th scope="col">Colorado</th>
      <th scope="col">New York</th>
      <th scope="col">California</th>
    </tr>
    <tr>
      <th scope="row">State Bird</th>
      <td class="fontStyle1">Northern Cardinal</td>
      <td class="fontStyle1">Robin Redbreast</td>
      <td class="fontStyle1">Lark Bunting</td>
      <td class="fontStyle1">Eastern Bluebird</td>
      <td class="fontStyle1">California Quail</td>
    </tr>
    <tr>
      <th scope="row">State Flower</th>
      <td class="fontStyle2">Red Carnation</td>
      <td class="fontStyle2">Apple Blossom</td>
      <td class="fontStyle2">Rocky Mounatin Columbine</td>
      <td class="fontStyle2">Rose</td>
      <td class="fontStyle2">California Poppy</td>
    </tr>
    <tr>
      <th scope="row">State Tree</th>
      <td class="fontStyle3">Ohio Buckeye</td>
      <td class="fontStyle3">Eastern White Pine</td>
      <td class="fontStyle3">Colorado Blue Spruce</td>
      <td class="fontStyle3">Sugar Maple</td>
      <td class="fontStyle3">California Redwood</td>
    </tr>
  </tbody>
</table>
	<p><a href="#header">Back to top</a></p>

	
<table width="200" border="1" id="s6">
  <tbody>
    <tr>
      <td colspan="5">Meals</td>
      
    </tr>
    <tr class="bonusMeme">
	  <td colspan="2">&nbsp;</td>
      <td>Brakefast</td>
      <td>Lunch</td>
      <td>Dinner</td>
    </tr>
    <tr>
      <td rowspan="4">Food</td>
      <td>Bread</td>
      <td>Roll</td>
      <td>Sliced Bread</td>
      <td class="bonusMeme2">Garlic Bread</td>
    </tr>
    <tr>
 
      <td>Main Course</td>
      <td>Eggs</td>
      <td>Ham Sandwitch</td>
      <td class="bonusMeme2">Spaghetti</td>
    </tr>
    <tr>
      <td>Vegetable</td>
      <td>Tomato</td>
      <td>Salad</td>
      <td class = "bonusMeme2">Bruusel Sprouts</td>
    </tr>
    <tr>
      <td>Dessert</td>
      <td class="bonusMeme3">N/A</td>
      <td>Cookies</td>
      <td class="bonusMeme2">Ice Cream</td>
    </tr>
  </tbody>
</table>
	<p><a href="#header">Back to top</a></p>

</body>
</html>